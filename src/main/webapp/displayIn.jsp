<%--
  Created by IntelliJ IDEA.
  User: 要努力学习啊
  Date: 2022/6/9
  Time: 19:34
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人信息展示</title>
</head>
<body>
<div style="position: center">
    <table>
        <tr>
            <th>学生ID</th>
            <th>学生姓名</th>
            <th>学生性别</th>
            <th>学生入学日期</th>
            <th>学生电话</th>
            <th>学生所在的学期</th>
            <th>学生状态</th>

        </tr>
        <c:forEach items="${users}" var="student">
            <tr>
                <td name="studentId">${student.id}</td>
                <td>${student.name}</td>
                <td>${student.sex}</td>
                <td>${student.date}</td>
                <td>${student.phone}</td>
                <td>${student.semester}</td>
                <td>${student.state}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
