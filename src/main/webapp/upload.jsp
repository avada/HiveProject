<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<style>
    .div1{
        background-color: antiquewhite;
        border-radius: 20px;
        box-shadow: 0 20px 20px rgba(0, 0, 0, 0.1), 0 6px 6px rgba(0, 0, 0, 0.1);
        padding: 80px 50px;
        text-align: center;
        max-width: 100%;
        width: 600px;
        margin:0 auto;
        margin-top: 40px;
    }
</style>
<head>
    <title>批量上传</title>
</head>
<script src="js/jquery.js"></script>
<body style="background-color: bisque;">
<script>
    function load(){
        var formData=new FormData();
        formData.append("file",$("#file_input")[0].files[0]);

        $.ajax({
            type: "post",
            async: true,
            url: "/UploadServlet",
            data:formData,
            contentType:false,
            processData: false,
            success:function (data){
                alert(data);
            },
            error:function (data){
                alert(data);
            }
        })
    }
</script>

<div class="div1">
    选择要批量插入的文件<input type="file" class="form-control" id="file_input">
    <button onclick="load()" >提交</button>
</div>
</body>
</html>

