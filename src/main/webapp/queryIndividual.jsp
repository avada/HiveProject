<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/6/7
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<style>
    #library{
        background-color: antiquewhite;
        border-radius: 20px;
        box-shadow: 0 20px 20px rgba(0, 0, 0, 0.1), 0 6px 6px rgba(0, 0, 0, 0.1);
        padding: 80px 50px;
        text-align: center;
        max-width: 100%;
        width: 600px;
        margin:0 auto;
        margin-top: 40px;
    }
</style>
<body style="background-color:bisque ;">

<div style="position: center">
    <form action="/QueryIndividual" method="get">
        <div id="library">
            <table width="430">
                <tr>

                    <td>ID query:</td>
                    <td align="center"><input type="text" name="studentId"></td>
                </tr>
                <tr>

                    <td>Name query:</td>
                    <td align="center"><input type="text" name="studentName"></td>
                </tr>
                <tr>

                    <td>Contact number:</td>
                    <td align="center"><input type="text" name="studentPhone"></td>
                </tr>
                <tr>

                    <td>Student status:</td>
                    <td align="center">
                        <select name="status" id="select" runat="server">
                            <option value="">ALL</option>
                            <option value="A">A</option>
                            <option value="L">L</option>
                        </select>
                    </td>
                </tr>
                <tr>

                    <td>Semester:</td>
                    <td align="center"><input type="text" name="studentSemester"></td>
                </tr>
                <tr>

                    <td>Enrollment time:</td>
                    <td align="center"><input type="text" name="studentEnrollment"></td>
                </tr>
            </table>
            <br/><br/>
            <button class="btn" type="submit">查询</button>
        </div>
    </form>
</div>
</body>
</html>

