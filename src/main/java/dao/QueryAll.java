package dao;

import bean.User;
import utills.HiveJdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Keven-赵仕星
 * @date 2022/6/6 16:45
 */

/**
 * 获取全部数据
 * 且json封装
 **/
public class QueryAll {

    static Connection connection;

    public QueryAll() {
        try {
            Connection connection = HiveJdbc.getConnection();
            if (connection != null)
                System.out.println("connect success");
            else
                System.out.println("connect fail");
            this.connection = connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> queryAll() throws Exception {
        QueryAll queryUser = new QueryAll();
        Statement stmt = connection.createStatement();
        String sql = "select * from student";
        ResultSet rus = stmt.executeQuery(sql);

        List<User> users = new ArrayList<>();
        while (rus.next()) {
            User user = new User(0, null, null, null, 0, null,null);
            user.setId(Integer.parseInt(rus.getString("id")));
            user.setName(rus.getString("name"));
            user.setDate(rus.getString("date"));
            user.setPhone(rus.getString("phone"));
            user.setSemester(rus.getInt("semester"));
            user.setState(rus.getString("state"));
            user.setSex(rus.getString("sex"));
            users.add(user);
            System.out.println(user);
        }
        HiveJdbc.close(stmt, connection);
        return users;
    }
}
