package dao;

import bean.User;
import utills.HiveJdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liu
 * @date 2022/6/7 21:45
 **/
public class QueryGrade {
    public Map<String,String> QueryGrade() throws SQLException {
        Connection connection = HiveJdbc.getConnection() ;
        Statement stmt = connection.createStatement();
        String sql = "select semester,count(*) as count from student group by semester ";
        ResultSet res = stmt.executeQuery(sql);
        Map<String,String> grades = new HashMap<>() ;
        while (res.next()) {
            grades.put(res.getString("semester"),res.getString("count"));
        }
        HiveJdbc.close(stmt, connection);
        return grades;
    }
}
