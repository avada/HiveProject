package dao;

import bean.User;
import utills.HiveJdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Liu
 * @date 2022/6/7 21:45
 **/
public class QuerySex {
    public Map<String,String> QuerySex() throws SQLException {
        Connection connection = HiveJdbc.getConnection() ;
        Statement stmt = connection.createStatement();
        String sql = "select sex,count(*) as count from student group by sex ";
        ResultSet res = stmt.executeQuery(sql);
        Map<String,String> sexs = new HashMap<>() ;
        while (res.next()) {
            if(res.getString("sex").equals("1") ){
                sexs.put("man",res.getString("count"));
            }else{
                sexs.put("woman",res.getString("count"));
            }
        }
        HiveJdbc.close(stmt, connection);
        return sexs;
    }
}
