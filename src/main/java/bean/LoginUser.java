package bean;

public class LoginUser {
    private String ID;
    private String Password;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public LoginUser() {
    }
    public LoginUser(String ID, String password) {
        this.ID = ID;
        Password = password;
    }


}
