package bean;

/**
 * @author 杨铭
 * 2022/6/5,22:24
 */
/**
 * 实体类
 * */
public class User {
    private int id;
    private String name;
    private String date;
    private String phone;
    private int semester;
    private String state;
    private String sex ;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public User(int id, String name, String date, String phone, int semester, String state, String sex) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.phone = phone;
        this.semester = semester;
        this.state = state;
        this.sex = sex;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + id +
                ", Name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", phone='" + phone + '\'' +
                ", semester=" + semester +
                ", state='" + state + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
