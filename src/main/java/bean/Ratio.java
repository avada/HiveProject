package bean;

/**
 * @author Liu
 * @date 2022/6/8 0:53
 **/
public class Ratio {
    private String name ;
    private Integer value ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Ratio{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
