package utills;

import bean.User;

import java.util.List;

/**
 * @author cheng
 * @date 2020/2/10 22:41
 */
public class ListObject {

    private List<User>  Items;
    private String code;
    private String Msg;
    private Integer length ;

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public List getItems() {
        return Items;
    }

    public void setItems(List items) {
        Items = items;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }




}
