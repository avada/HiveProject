package utills;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Created by 杨铭
 * 2022/06/5
 * 15:31
 */
public class HiveJdbc {
    private static String url = null;
    private static String user = null;
    private static String password = null;
    private static String dv = null;



    //  // Hive 0.11.0版本以前
    //  //private static String driverName = "org.apache.hadoop.hive.jdbc.HiveDriver";
    //  // Hive 0.11.0版本以后
    //  private static  String driverName = "org.apache.hive.jdbc.HiveDriver";
    //
    //  // Hive 0.11.0版本以前 jdbc:hive
    ////  private static String url = "jdbc:hive2://172.16.48.24:10000/tmp";
    //  // Hive 0.11.0版本以后jdbc:hive2
    //  private static String url = "jdbc:hive2://172.16.48.24:10000/tmp";
    //
    //

    // Hive - JDBC 驱动名及数据库 URL
//    public static final String Hive_URL = "jdbc:hive2://192.168.24.129:10000/tmp";
    static final String Hive_JDBC_DRIVER =  "org.apache.hive.jdbc.HiveDriver";
    public static final String Hive_URL ="jdbc:hive2://192.168.10.133:10000/hive";



    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "";

    static{
        Properties prop = new Properties();
//        InputStream in = JdbcUtils.class.getResourceAsStream("a");
        //不能是a.properties，因为在IDEA中文件名字就是a,与eclipse不同

        try {
            //以下四步避免了以后数据库名字等信息改变后来修改源码


            url  = Hive_URL;
            user  = USER;
            password   = PASS ;
            dv = Hive_JDBC_DRIVER;

            //注册驱动
            Class.forName(dv);
//        } catch (IOException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(url,user,password);
        return connection;

    }

    public static void close(Statement statement, Connection connection){
       // 编译 的连接  ；执行sql 关闭
        if(statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        // mysql 连接   ； 关掉
        if(connection != null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


}
