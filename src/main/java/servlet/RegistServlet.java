package servlet;

import bean.LoginUser;
import dao.QueryLogin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/regist")
public class RegistServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
              String id=req.getParameter("id");
              String password=req.getParameter("password");

        LoginUser loginUser=new LoginUser();
        loginUser.setID(id);
        loginUser.setPassword(password);

        QueryLogin queryLogin=new QueryLogin();
        queryLogin.insert(loginUser);


        PrintWriter pw= resp.getWriter();
        pw.write("注册成功!");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       resp.sendRedirect("/login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }
}
