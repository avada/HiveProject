package servlet; /**
 * @author 杨铭
 * 2022/6/5,22:22
 */

//import dao.QueryUser;

import bean.LoginUser;
import dao.QueryLogin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "LoginServlet", value = "/LoginServlet")
            public class LoginServlet extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                   String name=request.getParameter("name");
                   String password=request.getParameter("password");
            LoginUser user=new LoginUser(name,password);
                QueryLogin queryLogin=new QueryLogin();
                if(queryLogin.search(user)){
                    response.sendRedirect("/Menu.html");
                }else {
                    response.sendRedirect("/login.jsp");
                }


        }

        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
