package servlet;

import bean.Ratio;
import com.google.gson.Gson;
import service.Query;
import service.impl.QueryImpl;
import utills.JsonUtils;
import utills.ListObject;
import utills.ResponseUtils;
import utills.StatusCode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Liu
 * @date 2022/6/8 0:41
 **/
@WebServlet("/gradeRatioServlet")
public class gradeRatioServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Query query = new QueryImpl() ;
        PrintWriter printWriter = resp.getWriter() ;
        try {
            Map<String,String> grade =  query.queryGrade() ;
            List<String> result = new ArrayList<>();
            for (String key : grade.keySet()) {
                result.add(grade.get(key)) ;
            }
            Gson gson = new Gson() ;
            System.out.println(gson.toJson(result));
            printWriter.print(gson.toJson(result));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
