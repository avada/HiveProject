package servlet;

import bean.User;
import service.Query;
import service.impl.QueryImpl;
import utills.JsonUtils;
import utills.ListObject;
import utills.ResponseUtils;
import utills.StatusCode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author Keven-赵仕星
 * @date 2022/6/7 23:01
 */
@WebServlet("/QueryIndividual")
public class QueryIndividualServlet extends HttpServlet {

    public void init(HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("1111");
        Query query = new QueryImpl();
//        User searchUser=new User(req.getParameter("studentId"),req.getParameter("studentName"),req.getParameter("studentPhone"),
//                req.getParameter("status"),req.getParameter("studentSemester"),req.getParameter("studentEnrollment"));
        User searchUser=new User();
        searchUser.setId(Integer.parseInt(req.getParameter("studentId")));
        searchUser.setName(req.getParameter("studentName"));
        searchUser.setPhone(req.getParameter("studentPhone"));
        searchUser.setState(req.getParameter("status"));
        searchUser.setSemester(Integer.parseInt(req.getParameter("studentSemester")));
        searchUser.setDate(req.getParameter("studentEnrollment"));
        searchUser.setSex(null);
        System.out.println(searchUser);
        try {
            List<User> users = query.queryIndividual(searchUser);
            req.setAttribute("users",users);
            req.getRequestDispatcher("/displayIn.jsp").forward(req,resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
