package service;
import bean.User;
import servlet.QueryIndividualServlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 杨铭
 * 2022/6/5,22:30
 */
public interface Query {
    public List<User> queryAll() throws Exception ;

    public Map<String,String> querySex() throws Exception ;

    public Map<String,String> queryGrade() throws Exception ;

    public List<User> queryIndividual(User user) throws Exception ;
}
