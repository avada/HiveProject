package service;



import javax.servlet.http.HttpServletResponse;

/**
 * @author 杨铭
 * 2022/2/7,20:44
 */
public interface HadoopService {
    public void upload(String fileUrl);
    public void ajax(HttpServletResponse response, String code, String msg);
}
