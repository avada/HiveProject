package service.impl;
import bean.User;
import dao.QueryAll;
import dao.QueryGrade;
import dao.QueryIndividual;
import dao.QuerySex;
import service.Query;

import java.util.List;
import java.util.Map;

/**
 * @author 杨铭
 * 2022/6/5,22:28
 */
public class QueryImpl implements Query {
    /**
     * 查询全部数据
     * **/
    @Override
    public List<User> queryAll() throws Exception {
        QueryAll queryAll=new QueryAll();
        List<User> users=queryAll.queryAll();
        return users;
    }



    /**
     * 查询全校男女数量
     * **/
    @Override
    public Map<String,String> querySex() throws Exception {
        QuerySex querySex = new QuerySex() ;
        Map<String,String> sexs = querySex.QuerySex() ;
        return sexs;
    }

    /**
     * 查询全校年级人数
     * **/
    @Override
    public Map<String, String> queryGrade() throws Exception {
        QueryGrade queryGrade = new QueryGrade() ;
        Map<String,String> grades = queryGrade.QueryGrade() ;
        return grades;
    }

    @Override
    public List<User> queryIndividual(User user) throws Exception {
        QueryIndividual queryIndividual = new QueryIndividual() ;
        List<User> users = queryIndividual.queryIndividual(user) ;
        return users ;
    }
}
